package window;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import controller.Profile;
import controller.ProfileController;
import strip.LedStripModule;
import strip.LedStripSingleColor;
import strip.Sides.Side;
import strip.screenTranscoder.ScreenCapturer;

/**
 * Yet-to-be CLI.
 * Currently allows testing hardcoded profile with strips.
 * @author HHV
 *
 */
public class CommandLineInterface 
{	
	private static enum MenuMode
	{
		MenuMain,
		MenuProfile,
		MenuStrips
	}
	
	
	private static enum MenuMain
	{
		CLEAR("Wyczy�� ustawienia"),
		LOAD("Wczytaj ustawienia"),
		SAVE("Zapisz ustawienia"),
		PROFILE("Modyfikuj profil"),
		STRIPS("Modyfikuj ta�my"),
		RUN("Rozpocznij dzia�anie"),
		STOP("Zatrzymaj dzia�anie"),
		QUIT("Zako�cz");
		
		String entry;
		private MenuMain(String entry)
		{
			this.entry = entry;
		}
		
		public Integer getIndex()
		{
			return ordinal() + 1;
		}
		
		public static MenuMain fromIndex(Integer index) throws IllegalArgumentException
		{
			--index;
			for (MenuMain v :values())
				if(index == v.ordinal())
					return v;
			throw new IllegalArgumentException("Bad Side index!");
		}
		
		@Override
		public String toString()
		{
			return getIndex() + ". " + entry;
		}
		
		public static String getAllEntries()
		{
			StringBuilder sb = new StringBuilder();
			for(MenuMain m: values())
				sb.append(m+"\n");
			sb.deleteCharAt(sb.length()-1);
			return sb.toString();
		}
		
	}
		
	private static enum MenuProfile
	{
		ID("Zmie� MCU ID"),
		SERIAL("Zmi�� port szeregowy"),
		FPS("Zmie� ilo�� FPS"),
		GAMMA("Ustaw korekcj� gamma"),
		BACK("Powr�t");
		
		String entry;
		private MenuProfile(String entry)
		{
			this.entry = entry;
		}

		public Integer getIndex()
		{
			return ordinal() + 1;
		}
		
		public static MenuProfile fromIndex(Integer index) throws IllegalArgumentException
		{
			--index;
			for (MenuProfile v :values())
				if(index == v.ordinal())
					return v;
			throw new IllegalArgumentException("Bad Side index!");
		}
		
		@Override
		public String toString()
		{
			return getIndex() + ". " + entry;
		}
		
		public static String getAllEntries()
		{
			StringBuilder sb = new StringBuilder();
			for(MenuProfile m: values())
				sb.append(m+"\n");
			sb.deleteCharAt(sb.length()-1);
			return sb.toString();
		}
	}
	
	private static enum MenuStrips
	{
		ADD_COLOR("Dodaj ta�m� o ustalonym kolorze"),
		ADD_MODULE("Dodaj ta�m� ambilight"),
		REMOVE("Usu� ta�m�"),
//		MOVE("Przesu� tasm�"),
		BACK("Powr�t");
		
		String entry;
		private MenuStrips(String entry)
		{
			this.entry = entry;
		}
		
		public Integer getIndex()
		{
			return ordinal() + 1;
		}
		
		public static MenuStrips fromIndex(Integer index) throws IllegalArgumentException
		{
			--index;
			for (MenuStrips v :values())
				if(index == v.ordinal())
					return v;
			throw new IllegalArgumentException("Bad Side index!");
		}
		
		@Override
		public String toString()
		{
			return getIndex() + ". " + entry;
		}
		
		public static String getAllEntries()
		{
			StringBuilder sb = new StringBuilder();
			for(MenuStrips m: values())
				sb.append(m+"\n");
			sb.deleteCharAt(sb.length()-1);
			return sb.toString();
		}
	}
	
//	static final String INSERT_FILE_NAME	= "Podaj nazw� pliku: ";
	
	static final String INSERT_MCU_ID		= "Podaj identyfikator kontrolera: ";
	static final String INSERT_SERIAL		= "Podaj nazw� portu szeregowego: ";
	static final String INSERT_FPS			= "Podaj docelow� ilo�� FPS: ";
	static final String INSERT_GAMMA		= "W��czy� korekcj� gamma (t/n)?: ";
//	static final Character INSERT_GAMMA_YES	= 't';
//	static final Character INSERT_GAMMA_NO	= 'n';
	
	static final String INSERT_LED_COUNT	= "Podaj ilo�� diod: ";
	static final String INSERT_RED			= "Podaj warto�� koloru czerwonego (0-255): ";
	static final String INSERT_GREEN		= "Podaj warto�� koloru zielonego (0-255): ";
	static final String INSERT_BLUE			= "Podaj warto�� koloru niebieskiego (0-255): ";
	static final String INSERT_SCREEN_ID	= "Podaj numer monitora: ";
	static final String INSERT_SIDE			= "Wybierz stron�: ";
	static final String INSERT_PREFIX		= "Podaj ilo�� niewidzialnych di�d przed ta�m� (prefix): ";
	static final String INSERT_SUFFIX		= "Podaj ilo�� niewidzialnych di�d za ta�m� (suffix): ";
//	static final String INSERT_TRANSCODER	= "Wybierz transkoder: ";
	static final String INSERT_INDEX		= "Podaj indeks ta�my: ";
			
	static final String CURRENT_CONFIG		= "Aktualna konfiguracja: ";
	static final String INVALID_MENU_MODE	= "Spr�bowano wy�wietli� nie istniej�ce menu!";
	static final String ERR_INS_INVALID_VAL	= "Podano b��dn� warto��!";
	static final String ERROR_SAVING_FILE	= "Nie uda�o si� zapisa� profilu!";
	static final String ERROR_LOADING_FILE	= "Nie uda�o si� odczyta� profilu!";
	static final String DEFAULT_PROFILE_SAVE= "./save.dat";
	
	Profile profile;
	ProfileController pc;
	MenuMode menuMode;
	
	void printInfo()
	{
		System.out.println(CURRENT_CONFIG);
		System.out.println(profile);
		System.out.println(profile.getStrip());
		System.out.println("");
	}
	
	void processMenuMain(Scanner sc)
	{
		if(!sc.hasNextLine())
			return;
		
		String cin = sc.nextLine();

		if(cin.length() == 0)
			return;
		
		MenuMain menu;
		try
		{
			menu = MenuMain.fromIndex(Integer.parseInt(cin));
		}
		catch (IllegalArgumentException e) {
			System.out.println("Error: " + e.getMessage());
			return;
		}
		
		switch (menu)
		{
			case CLEAR:
				profile = new Profile();
				pc = new ProfileController(profile);
				break;
			case PROFILE:
				menuMode = MenuMode.MenuProfile;
				printInfo();
				System.out.println(MenuProfile.getAllEntries());
				return;
			case STRIPS:
				menuMode = MenuMode.MenuStrips;
				printInfo();
				System.out.println(MenuStrips.getAllEntries());
				return;
			case QUIT:
				pc.stopContinuous();
				pc.stop();
				System.exit(0);
			case STOP:
				pc.stop();
				break;
			case RUN:
				//if already running, turn off and on again
				if(pc.isRunningContinuous())
					pc.stop();
				try
				{
					pc.start();
				}
				catch (IllegalArgumentException | NullPointerException | IOException e)
				{
					System.out.println("Error: " + e.getMessage());
					return;
				}
				pc.runContinuous();
				break;
			case LOAD:
				try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(DEFAULT_PROFILE_SAVE)))
				{
					profile = (Profile) ois.readObject();
				}
				catch (IOException | ClassNotFoundException e)
				{
					System.out.println("Error: " + ERROR_LOADING_FILE);
				}
				break;
			case SAVE:
				try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DEFAULT_PROFILE_SAVE)))
				{
					oos.writeObject(profile);
				}
				catch (IOException e)
				{
					System.out.println("Error: " + ERROR_SAVING_FILE);
					e.printStackTrace();
				}
				break;
			default:
				break;
		}
		printInfo();
		System.out.println(MenuMain.getAllEntries());
	}
	
	void processMenuProfile(Scanner sc)
	{	
		if(!sc.hasNextLine())
			return;
		
		String cin = sc.nextLine();
		
		if(cin.length() == 0)
			return;
		
		MenuProfile menu;
		try
		{
			menu = MenuProfile.fromIndex(Integer.parseInt(cin));
		}
		catch (IllegalArgumentException e) {
			System.out.println("Error: " + e.getMessage());
			return;
		}
		
		switch (menu)
		{
			case BACK:
				menuMode = MenuMode.MenuMain;
				printInfo();
				System.out.println(MenuMain.getAllEntries());
				return;
			case ID:
				do {
					System.out.println(INSERT_MCU_ID);
					while(!sc.hasNextLine());
					cin = sc.nextLine();
					//TODO: proper no ID and escape loop processing
					if(cin.length()==0)
						break;
				} while(!Profile.isValidMcuId(cin));
				profile.setMcuId(cin);
				break;
			case SERIAL:
				do {
					System.out.println(INSERT_SERIAL);
					while(!sc.hasNextLine());
					cin = sc.nextLine();
					
					if(cin.length()==0)
						break;
				} while(!Profile.isValidComPort(cin));
				profile.setComPort(cin);
				break;
			case FPS:
			
				System.out.println(INSERT_FPS);
				while(!sc.hasNextLine());
				cin = sc.nextLine();
				int fps = Integer.parseInt(cin);
				if(fps < 0 || fps >255)
					break;
				profile.setFps(fps);
				break;
			case GAMMA:
				System.out.println(INSERT_GAMMA);
				while(!sc.hasNextLine());
				cin = sc.nextLine();
				if(cin.toLowerCase().startsWith("t"))
					profile.setGammaCorrectionEnabled(true);
				else if(cin.toLowerCase().startsWith("n"))
					profile.setGammaCorrectionEnabled(false);
				break;
			default:
				break;
		}
		
		printInfo();
		System.out.println(MenuProfile.getAllEntries());
		
	}
	
	void processMenuStrips(Scanner sc)
	{
		if(!sc.hasNextLine())
			return;
		
		printInfo();
		System.out.println(MenuStrips.getAllEntries());
		
		String cin = sc.nextLine();

		if(cin.length() == 0)
			return;
		
		MenuStrips menu;
		try
		{
			menu = MenuStrips.fromIndex(Integer.parseInt(cin));
		}
		catch (IllegalArgumentException e) {
			System.out.println("Error: " + e.getMessage());
			return;
		}
		
		switch (menu)
		{
			case BACK:
				menuMode = MenuMode.MenuMain;
				printInfo();
				System.out.println(MenuMain.getAllEntries());
				return;
			case ADD_COLOR:
			{
				System.out.println(INSERT_LED_COUNT);
				while(!sc.hasNextLine());
				Integer cnt = Integer.parseInt(sc.nextLine());
				if(cnt<=0)
				{
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}


				System.out.println(INSERT_RED);
				while(!sc.hasNextLine());
				Integer r = Integer.decode(sc.nextLine());
				if((r&0xff)!=r)
				{
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}

				System.out.println(INSERT_GREEN);
				while(!sc.hasNextLine());
				Integer g = Integer.decode(sc.nextLine());
				if((g&0xff)!=g)
				{
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}
				
				System.out.println(INSERT_BLUE);
				while(!sc.hasNextLine());
				Integer b = Integer.decode(sc.nextLine());
				if((b&0xff)!=b)
				{
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}
				
				profile.addStrip(new LedStripSingleColor(cnt,r,g,b));
				break;
			}
			case ADD_MODULE:
			{
				System.out.println(INSERT_LED_COUNT);
				while(!sc.hasNextLine());
				Integer cnt = Integer.parseInt(sc.nextLine());
				if(cnt<=0)
				{
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}
				
				System.out.println(INSERT_SCREEN_ID);
				while(!sc.hasNextLine());
				Integer scr = Integer.parseInt(sc.nextLine());
				if(scr<0 || scr>=ScreenCapturer.getScreenCount())
				{
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}
				
				System.out.println(INSERT_SIDE);
				while(!sc.hasNextLine());
				Side sid;
				try{
					sid = Side.fromIndex(Integer.parseInt(sc.nextLine()));
				}
				catch (IllegalArgumentException e) {
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}
				
				System.out.println(INSERT_PREFIX);
				while(!sc.hasNextLine());
				Integer pre = Integer.parseInt(sc.nextLine());
				if(pre<0)
				{
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}
				
				System.out.println(INSERT_SUFFIX);
				while(!sc.hasNextLine());
				Integer post = Integer.parseInt(sc.nextLine());
				if(post<0)
				{
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}
				profile.addStrip(new LedStripModule(scr,sid,cnt,pre,post));
				break;
			}
			case REMOVE:
			{
				System.out.println(INSERT_INDEX);
				while(!sc.hasNextLine());
				Integer idx = Integer.parseInt(sc.nextLine());
				if(idx<1||idx>profile.getStrip().getStripCount())
				{
					System.out.println(ERR_INS_INVALID_VAL);
					break;
				}
				profile.getStrip().getStripSeries().remove(idx-1);
				break;
			}
			default:
				break;
		}
		
		printInfo();
		System.out.println(MenuStrips.getAllEntries());
	}
	
	void loadProfile()
	{
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(DEFAULT_PROFILE_SAVE)))
		{
			profile = (Profile) ois.readObject();
		}
		catch (IOException | ClassNotFoundException e)
		{
			profile = new Profile("66dff545348836767062449");
			profile.addStrip(new LedStripModule(1, Side.TOP_RIGHT_TO_LEFT,40));
		}
		pc = new ProfileController(profile);
		
//		profile.addStrip(new LedStripSingleColor(10, 0,0,10));
//		profile.addStrip(new LedStripSingleColor(10, 0,10,0));
//		profile.addStrip(new LedStripSingleColor(10, 10,0,0));
//		profile.addStrip(new LedStripSingleColor(10, 0,0,0));
		
	}
	
	public CommandLineInterface()
	{
		loadProfile();
		
		menuMode = MenuMode.MenuMain;
		printInfo();
		System.out.println(MenuMain.getAllEntries());
		
		try(Scanner sc = new Scanner(System.in))
		{			
			while(true)
			{
				//TODO: handle interrupts, i.e. controller disconnected
				switch (menuMode)
				{
					case MenuMain:
						processMenuMain(sc);
						break;
					case MenuProfile:
						processMenuProfile(sc);
						break;
					case MenuStrips:
						processMenuStrips(sc);
						break;
					default:
						System.err.println(INVALID_MENU_MODE);
						menuMode = MenuMode.MenuMain;
						break;
				}
			}
		}
		
	}
	public static void main(String[] args) {
		new CommandLineInterface();
//		new TranscoderTestFrame(0, Side.TOP, 40);
	}
}
