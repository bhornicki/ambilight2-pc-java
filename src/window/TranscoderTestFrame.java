package window;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import strip.Sides.Side;
import strip.screenTranscoder.DefaultTranscoder;
import strip.screenTranscoder.ImageTranscoder;

/**
 * Currently not in use.
 * Will provide feedback with transcoder color output to user. 
 * @author HHV
 *
 */
public class TranscoderTestFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	private Integer ledCount;
	private Integer screenID;
	private Side location;

	private class TranscoderTestPanel extends JPanel 
	{

		private static final long serialVersionUID = 1L;
		ImageTranscoder transc;

		public TranscoderTestPanel(final Integer screenID, final Side location, final int ledCnt) {
			transc = new DefaultTranscoder();
			if (!location.isVertical())
				setSize(ledCount * 8, 100);
			else
				setSize(100, ledCount * 8);
			setMinimumSize(getSize());
			setMaximumSize(getSize());
//			setVisible(true);
		}

		@Override
		protected void paintComponent(Graphics g) {
			Color[] ret = transc.getValues(screenID, location.getSimple(), ledCount);
			if (ret.length != ledCount)
				System.err.println("ret.length!=ledCount");
			if (!location.isVertical()) {
				for (int i = 0; i < ret.length; i++) {
					Color col = ret[i];
					g.setColor(col);
					g.fillRect(i * 8, 0, 8, 100);
				}
			} else {
				for (int i = 0; i < ret.length; i++) {
					Color col = ret[i];
					g.setColor(col);
					g.fillRect(0, i * 8, 100, 8);
				}
			}
		}
	}
	public TranscoderTestFrame(final Integer screenID, final Side location, final int ledCnt)
	{
		this.location = location;
		this.screenID = screenID;
		ledCount = ledCnt;
		JPanel panel = new TranscoderTestPanel(screenID, location, ledCnt);
		setContentPane(panel);
		setSize(panel.getSize());
		setVisible(true);
	}
	public void update()
	{
		repaint();
	}
}
