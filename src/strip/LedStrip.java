package strip;

import java.awt.Color;
import java.io.Serializable;

/**
 * Common interface for LED strips 
 * 
 * @author HHV
 *
 */
public abstract class LedStrip implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * ammount of bytes per one WS2812 LED. Equivalently describes amount of
	 * LEDs/colors in single WS2812 package 
	 * 
	 * TODO: add support for RGBW LEDS
	 */
	private static final Integer BYTES_PER_LED=3;
	
	/**
	 * ammount of WS2812 LEDs in a string
	 */
	private Integer ledCount=0;

	/**
	 * Returns string made of HEX LED values.
	 * 
	 * Structure: "RR,GG,BB;" repeated ledCount times.
	 * 
	 * Strings can be concatenated. To send string to MCU, prepend it with
	 * CMD_SEND_LINE_HEX and append with newline.
	 * 
	 * Usage of ScreenCapturer.syncCapture() may be necessary to make it work
	 * properly.
	 * 
	 * 
	 * @return string made of HEX LED values
	 */
	public abstract String getStringOfBytes();
	
	/**
	 * Returns array of LED colors.
	 * 
	 * Usage of ScreenCapturer.syncCapture() may be necessary to make it work
	 * properly.
	 * 
	 * @return array of LED colors
	 */
	public abstract Color[] getColors();
		
	/**
	 * Converts color to default sRGB ColorModel (Bits 24-31 are alpha, 16-23 are
	 * red, 8-15 are green, 0-7 are blue) with values changed to make alpha channel
	 * value equal to 0.
	 * 
	 * @param c color to be converted
	 * @return default sRGB ColorModel with alpha channel == 0
	 */
	static public Integer colorToRgb(Color c)
	{
		return rgbaToRgb(c.getRGB());
	}
	
	/**
	 * Converts default sRGB ColorModel (Bits 24-31 are alpha, 16-23 are red, 8-15
	 * are green, 0-7 are blue) to sRGB ColorModel with alpha channel value equal to
	 * 0.
	 * 
	 * @param RGBA default sRGB ColorModel
	 * @return default sRGB ColorModel with alpha channel == 0
	 */
	static public Integer rgbaToRgb(Integer RGBA)
	{
		Integer res=0;
		Integer alpha = (RGBA >> 24)&0xFF;
		for (int i = 0; i < 3; i++)
		{
			res |= (((RGBA>>(i*8))&0xff) * alpha/255)<<(i*8);			
		}
		return res;
	}
	
	public LedStrip(Integer led_count)
	{
		ledCount=led_count;
	}
	
	public Integer getLedCount()
	{
		return ledCount;
	}
	
	public Integer getByteCount()
	{
		return ledCount*BYTES_PER_LED;
	}
	
	public Integer getBytesPerLed()
	{
		return BYTES_PER_LED;
	}
}
