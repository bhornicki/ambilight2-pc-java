package strip;

import java.awt.Color;


import strip.Sides.Side;
import strip.screenTranscoder.DefaultTranscoder;
import strip.screenTranscoder.ImageTranscoder;

/**
 * LED strip with screenshot as a source of colors for LEDs.
 * 
 * Usage of ScreenCapturer.syncCapture() is necessary to make this class work
 * properly.
 * 
 * @author HHV
 *
 */
public class LedStripModule extends LedStrip
{
	private static final long serialVersionUID = 1L;

	/**
	 * Transcoder used to process screenshots
	 */
	private ImageTranscoder transcoder;

	/**
	 * screenshot source screen
	 */
	private final Integer scrNr;
	
	/**
	 * indicates screen part and LED direction
	 */
	private final Side    scrSide;
	
	/**
	 * Determines how many non-existing LEDs will fit before real strip start.
	 * Useful when strip does not start exactly at monitor corner.
	 * Those LEDs DO NOT COUNT into strip led count.
	 */
	private final Integer prefix;
	/**
	 * Determines how many non-existing LEDs will fit after real strip end.
	 * Useful when strip does not end exactly at monitor corner.
	 * Those LEDs DO NOT COUNT into strip led count.
	 */
	private final Integer suffix;

	/**
	 * Changes current transcoder to new one
	 * @param transcoder ImageTranscoder instance
	 */
	public void setTranscoder(ImageTranscoder transcoder)
	{
		this.transcoder = transcoder;
	}

	/**
	 * Creates LED strip with colors based on screenshot
	 * 
	 * @param screenNr     Screen ID, starts from 0
	 * @param screenSide   Part of screen behind which strip is mounted in specified
	 *                     direction
	 * @param ledCnt       amount of LEDs in strip
	 * @param empty_prefix Adds "virtual LED strip" at the beginning of strip. Use
	 *                     this when strip does not start exactly at the edge of
	 *                     monitor.
	 * @param empty_suffix Adds "virtual LED strip" at the end of strip. Use this
	 *                     when strip does not start exactly at the edge of monitor.
	 * @param imgTrans     Transcoder which will process screenshot to a strip data
	 *                     array
	 */
	public LedStripModule(final Integer screenNr, final Side screenSide, final Integer ledCnt,
			final Integer empty_prefix, final Integer empty_suffix, ImageTranscoder imgTrans)
			throws IllegalArgumentException
	{
		super(ledCnt);

		if (ledCnt <= 0)
			throw new IllegalArgumentException("Passed bad led count");
		if (empty_prefix < 0)
			throw new IllegalArgumentException("Passed negative prefix");
		if (empty_suffix < 0)
			throw new IllegalArgumentException("Passed negative suffix");

		if (imgTrans == null)
			imgTrans = new DefaultTranscoder();

		this.scrNr = screenNr;
		this.scrSide = screenSide;
		this.transcoder = imgTrans;
		prefix = empty_prefix;
		suffix = empty_suffix;
	}

	/**
	 * Creates LED strip with colors based on screenshot with default transcoder
	 * 
	 * @param screenNr     Screen ID, starts from 0
	 * @param screenSide   Part of screen behind which strip is mounted in specified
	 *                     direction
	 * @param ledCnt       amount of LEDs in strip
	 * @param empty_prefix Adds "virtual LED strip" at the beginning of strip. Use
	 *                     this when strip does not start exactly at the edge of
	 *                     monitor.
	 * @param empty_suffix Adds "virtual LED strip" at the end of strip. Use this
	 *                     when strip does not start exactly at the edge of monitor.
	 */
	public LedStripModule(final Integer screenNr, final Side screenSide, final Integer ledCnt,
			final Integer empty_prefix, final Integer empty_suffix) throws IllegalArgumentException
	{
		this(screenNr, screenSide, ledCnt, empty_prefix, empty_suffix, new DefaultTranscoder());
	}

	/**
	 * Creates LED strip with colors based on screenshot with no LED prefix and
	 * suffix
	 * 
	 * @param screenNr   Screen ID, starts from 0
	 * @param screenSide Part of screen behind which strip is mounted in specified
	 *                   direction
	 * @param ledCnt     amount of LEDs in strip
	 * @param imgTrans   Transcoder which will process screenshot to a strip data
	 *                   array
	 */
	public LedStripModule(final Integer screenNr, final Side screenSide, final Integer ledCnt,
			final ImageTranscoder imgTrans) throws IllegalArgumentException
	{
		this(screenNr, screenSide, ledCnt, 0, 0, imgTrans);
	}

	/**
	 * Creates LED strip with colors based on screenshot with default transcoder, no
	 * LED prefix and suffix
	 * 
	 * @param screenNr   Screen ID, starts from 0
	 * @param screenSide Part of screen behind which strip is mounted in specified
	 *                   direction
	 * @param ledCnt     amount of LEDs in strip
	 */
	public LedStripModule(final Integer screenNr, final Side screenSide, final Integer ledCnt)
			throws IllegalArgumentException
	{
		this(screenNr, screenSide, ledCnt, 0, 0, new DefaultTranscoder());
	}

	@Override
	public String getStringOfBytes()
	{
		Color[] cols = getColors();
		
		StringBuilder sb = new StringBuilder(getByteCount() * 3); // 3 chars per byte
		for (Color c : cols)
			sb.append(String.format("%02x,%02x,%02x;", c.getRed(), c.getGreen(), c.getBlue()));
		return sb.toString();
	}

	@Override
	public Color[] getColors()
	{
		Color[] full = transcoder.getValues(scrNr, scrSide.getSimple(), getLedCount() + prefix + suffix);

		if (prefix > 0 || suffix > 0)
		{
			// we are reading from right to left, so select proper offset
			int offset = scrSide.isBackward() ? suffix : prefix;

			Color[] temp = full;
			full = new Color[getLedCount()];

			for (int i = 0; i < getLedCount(); i++)
				full[i] = temp[i + offset];
		}

		// if going from right to left or bottom to top, change order
		if (scrSide.isBackward())
		{
			int middle = full.length / 2;
			for (int i = 0; i < middle; i++)
			{
				Color c = full[i];
				full[i] = full[full.length - i-1];
				full[full.length - i-1] = c;
			}
		}
		return full;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("StripModule[");
		sb.append("count " + getLedCount() + ", ");
		sb.append("screen " + scrNr+ ", ");
		sb.append("side " + scrSide + ", ");
		if(!(transcoder instanceof DefaultTranscoder))
			sb.append("transcoder " + transcoder + ", ");
		if(prefix>0)
			sb.append("prefix " + prefix + ", ");
		if(suffix>0)
			sb.append("suffix " + suffix + ", ");
		
		sb.delete(sb.length()-2, sb.length());
		sb.append("]");
		
		return sb.toString();
	}
	
	
}
