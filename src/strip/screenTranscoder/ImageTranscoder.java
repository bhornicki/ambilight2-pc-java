package strip.screenTranscoder;

import java.awt.Color;

import strip.Sides.SimpleSide;

/**
 * Interface for processing screenshot to row of Colors used in LedStripModule.
 * 
 * 
 * @author HHV
 *
 */
public interface ImageTranscoder
{
	/**
	 * 
	 * @param screenID number assigned to screen which image will be processed,
	 *                 where first one is 0.
	 * @param location part of the screen used to create Color[]
	 * @param ledCnt   size of returned Color[] array
	 * @return array with data prepared for LedStripModule
	 */
	public Color[] getValues(final Integer screenID, final SimpleSide location, final int ledCnt);
}
