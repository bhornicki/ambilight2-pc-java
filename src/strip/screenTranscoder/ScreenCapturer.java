package strip.screenTranscoder;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;



/** 
 * Flyweight for screenshots
 * Used for capturing all screenshots at the same moment.
 * 
 * TODO: throw exception when screenID exceeds getScreenCount()
 * TODO: check if monitor wasn't unplugged during program operation
 * 
 * @author HHV
 */
public class ScreenCapturer
{
	/**
	 * Map with screen ID and most recent screenshot. All screenshots are updated
	 * when syncCapture is called.
	 */
	private static Map<Integer, BufferedImage> screenshots = new HashMap<Integer, BufferedImage>(3);

	/**
	 * Updates screenshots for every screenID stored in screenshots map 
	 */
	public static void syncCapture()
	{
		for (Map.Entry<Integer, BufferedImage> entry : screenshots.entrySet())
		{
			entry.setValue(getScreen(entry.getKey()));
		}
	}

	/**
	 * TODO: automatic remover based on usage of getScreen(ID)
	 * 
	 * This function removes screenID from capture list. Note that calling
	 * getScreen(screenID) will add it back.
	 * 
	 * @param screenID number assigned to screen, where first one is 0.
	 */
	public static void removeFromSyncCapture(Integer screenID)
	{
		screenshots.remove(screenID);
	}

	/**
	 * Returns Set with IDs of monitors with screenshot stored in flyweight
	 * 
	 * @return IDs of monitors stored in flyweight
	 */
	static Set<Integer> getCaptureSyncedSet()
	{
		return screenshots.keySet();
	}

	/**
	 * Returns screenshot stored in flyweight Map. If Map does not contain screenID
	 * - immediately creates new screenshot and adds screenID with screenshot to
	 * map.
	 * 
	 * @param screenID number assigned to screen, where first one is 0.
	 * @return screenshot from a given monitor synchronized by syncCapture()
	 */
	static BufferedImage getSyncScreen(Integer screenID)
	{
		BufferedImage result = screenshots.get(screenID);
		
		if(result == null)
		{
			screenshots.put(screenID,getScreen(screenID));
			result = screenshots.get(screenID);
		}
		
		return result;
	}

	/**
	 * helper function used to make actual screenshot
	 * 
	 * @param screenID number assigned to screen, where first one is 0.
	 * @return immediate screenshot from a given monitor
	 */
	private static BufferedImage getScreen(Integer screenID)
	{
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] screens = ge.getScreenDevices();
		if (screenID > screens.length)
			return null;

		Rectangle screenBounds = screens[screenID].getDefaultConfiguration().getBounds();
		try
		{
			Robot robot = new Robot();
			BufferedImage screenShot = robot.createScreenCapture(screenBounds);
			return screenShot;
		}
		catch (AWTException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	/**
	 * Returns count of screens connected to PC
	 * @return connected screen count
	 */
	public static Integer getScreenCount()
	{
		return GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices().length;
	}
}
