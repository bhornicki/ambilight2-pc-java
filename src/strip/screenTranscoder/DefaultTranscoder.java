package strip.screenTranscoder;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.Serializable;

import strip.LedStrip;
import strip.Sides.SimpleSide;

/**
 * Default way to process screenshot into Color[] array
 * 
 * TODO: change approximating area color multiplier from aspect ratio to custom one
 * 
 * @author HHV
 *
 */
public class DefaultTranscoder implements ImageTranscoder, Serializable 
{ 
	private static final long serialVersionUID = 1L;
	
	/**
	 * Alpha correction. Value between 0-255, will dim the color by ALPHA/255
	 */
	private static final int  ALPHA = 27;

	@Override
	public Color[] getValues(final Integer screenID, final SimpleSide location, final int ledCnt) {
		Color[] vals = new Color[ledCnt];
		int w, h;
		// w/h
		Float aspect = (float) ScreenCapturer.getSyncScreen(screenID).getWidth()
				/ ScreenCapturer.getSyncScreen(screenID).getHeight();
		if (location.isVertical()) {

			h = ledCnt;
			w = (int) ((h + aspect) * aspect);
		} else {
			w = ledCnt;
			h = (int) ((w + aspect) / aspect);
		}
		Image master =  ScreenCapturer.getSyncScreen(screenID).getScaledInstance(w, h,
				Image.SCALE_DEFAULT);
		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_3BYTE_BGR);
		switch (location) {
		case TOP:
			img.getGraphics().drawImage(master,0,0,w,1, 0,0,w,1,null);
			break;
		case BOTTOM:
			img.getGraphics().drawImage(master,0,0,w,1, 0, h - 1, w, 1,null);
			break;
		case LEFT:
			img.getGraphics().drawImage(master,0,0,1,h, 0, 0, 1, h,null);
			break;
		case RIGHT:
			img.getGraphics().drawImage(master,0,0,1,h, w - 1, 0, 1, h,null);
			break;

		}

		for (int i = 0; i < ledCnt; i++) {
			if (location.isVertical())
				vals[i] = new Color(LedStrip.rgbaToRgb(img.getRGB(0, i)&0x00FFFFFF|ALPHA<<24));
			else
				vals[i] = new Color(LedStrip.rgbaToRgb(img.getRGB(i, 0)&0x00FFFFFF|ALPHA<<24));
		}
		return vals;
	}
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName();
	}

}
