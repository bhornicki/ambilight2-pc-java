package strip.Sides;

/**
 * Defines which part of the screen is being used to acquire color information
 * for LEDs.
 * 
 * @author HHV
 *
 */
public enum SimpleSide
{
	TOP,
	BOTTOM,
	LEFT,
	RIGHT;
	
	public Boolean isVertical()
	{
		return (this==RIGHT||this==LEFT);
	}
}