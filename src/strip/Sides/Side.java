package strip.Sides;

/**
 * Enum which extends SimpleSide by containing screen part and direction
 * information used to acquire data for LEDs.
 * 
 * @author HHV
 *
 */
public enum Side
{
	TOP_LEFT_TO_RIGHT(SimpleSide.TOP, false),
	TOP_RIGHT_TO_LEFT(SimpleSide.TOP, true),
	BOTTOM_LEFT_TO_RIGHT(SimpleSide.BOTTOM, false),
	BOTTOM_RIGHT_TO_LEFT(SimpleSide.BOTTOM, true),
	LEFT_TOP_TO_BOTTOM(SimpleSide.LEFT, false),
	LEFT_BOTTOM_TO_TOP(SimpleSide.LEFT, true),
	RIGHT_TOP_TO_BOTTOM(SimpleSide.RIGHT, false),
	RIGHT_BOTTOM_TO_TOP(SimpleSide.RIGHT, true);

	private SimpleSide parent;
	private Boolean    isBackward;

	Side(SimpleSide parent, Boolean isBackward)
	{
		this.parent = parent;
		this.isBackward=isBackward;
	}

	public SimpleSide getSimple()
	{
		return parent;
	}

	public Boolean isVertical()
	{
		return parent.isVertical();
	}

	public Boolean isBackward()
	{
		return isBackward;
	}

	public Integer getIndex()
	{
		return ordinal() + 1;
	}
	
	public static Side fromIndex(Integer index) throws IllegalArgumentException
	{
		--index;
		for (Side v :values())
			if(index == v.ordinal())
				return v;
		throw new IllegalArgumentException("Bad Side index!");
	}
	
	@Override
	public String toString()
	{
		return getIndex() + ". " + name();
	}
	
	public static String getAllEntries()
	{
		StringBuilder sb = new StringBuilder();
		for(Side s: values())
			sb.append(s+"\n");
		sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}

}
