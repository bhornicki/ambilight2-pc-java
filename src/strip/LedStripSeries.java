package strip;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Class to merge multiple strips.
 * 
 * @author HHV
 *
 */
public class LedStripSeries implements Serializable
{
	private static final long serialVersionUID = 1L;
	/**
	 * Array with LED strips
	 */
	ArrayList<LedStrip> strips = new ArrayList<LedStrip>(1);
	
	/**
	 * Appends LED strip to array
	 * @param part LED strip to be added
	 * 
	 * #TODO: null check
	 */
	public void add(LedStrip part)
	{
		strips.add(part);
	}

	/**
	 * Returns count of WS2812's present in all added strips together
	 * @return total quantity of LEDs 
	 */
	public Integer getLedCount() 
	{
		Integer count=0;
		for (LedStrip module : strips) 
			count+=module.getLedCount();
		return count;
	}

	/**
	 * Returns total byte count present in LED strips
	 * @return total quantity of bytes
	 */
	public Integer getByteCount() {
		Integer count=0;
		for (LedStrip module : strips) 
			count+=module.getByteCount();
		return count;
	}
	
	/**
	 * Returns list with all strips
	 * @return list with all strips
	 */
	public ArrayList<LedStrip> getStripSeries() {
		return strips;
	}
	
	/**
	 * Returns ammount of managed strips  
	 * @return ammount of managed strips
	 */
	public Integer getStripCount() {
		return strips.size();
	}

	/**
	 * Concatenates stringOfBytes from each strip and returns it 
	 * @return string made of HEX LED values
	 */
	public final String getStringOfBytes()
	{
		StringBuilder sb = new StringBuilder();
		strips.forEach(i -> sb.append(i.getStringOfBytes()));
		return sb.toString();
	}

	/**
	 * Concatenates colors from each strip and returns it 
	 * @return string made of Colors
	 */
	public final Color[] getColors()
	{
		LinkedList<Color> ret = new LinkedList<Color>();
		strips.forEach(s-> {
			for(Color c : s.getColors())
				ret.add(c);
		});
		return (Color[]) ret.toArray();
	}

	public Integer getBytesPerLed()
	{
		//TODO: make me pretty
		if(strips.size()==0)
			return 3;
		return strips.get(0).getBytesPerLed();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int c = 1;
		for(LedStrip s:strips)
			sb.append((c++) +". "+s+"\n");
		//delete last \n
		if(sb.length()>0)
			sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
}
