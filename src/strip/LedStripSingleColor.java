package strip;

import java.awt.Color;


/**
 * LED strip with preconfigured constant color
 * 
 *  #TODO: (future:) create LedStripGradient
 * 
 * @author HHV
 *
 */
public class LedStripSingleColor extends LedStrip
{

	private static final long serialVersionUID = 1L;
	/**
	 * Color of LEDs in this strip
	 */
	Byte r, g, b;

	/**
	 * Creates led strip with all LEDs turned off
	 * 
	 * @param led_count ammount of LEDs in this strip
	 */
	public LedStripSingleColor(Integer led_count)
	{
		
		super(led_count);
		r = g = b = 0;
	}

	
	/**
	 * Creates strip with all LEDs set to (r,g,b).
	 * 
	 * #TODO: change bit mask to upper boundary, do sub-zero check, throw IllegalArgument
	 * 
	 * @param led_count ammount of LEDs in this strip
	 * @param r         red value (0-255)
	 * @param g         green value (0-255)
	 * @param b         blue value (0-255)
	 */
	public LedStripSingleColor(Integer led_count, Integer r, Integer g, Integer b)
	{
		super(led_count);
		this.r = (byte)(r & 0xFF);
		this.g = (byte)(g & 0xFF);
		this.b = (byte)(b & 0xFF);
	}

	/**
	 * Creates strip with all LEDs set to the same color, with alpha component used
	 * in calculations.
	 *
	 * @param led_count ammount of LEDs in this strip
	 * @param rgba      default sRGB ColorModel. (Bits 24-31 are alpha, 16-23 are
	 *                  red, 8-15 are green, 0-7 are blue).
	 */
	public LedStripSingleColor(Integer led_count, Integer rgba)
	{
		super(led_count);
		
		Integer rgb = rgbaToRgb(rgba);
		r = (byte)((rgb<<16)	& 0xFF);
		g = (byte)((rgb<<8)		& 0xFF);
		b = (byte)( rgb 		& 0xFF);
	}

	/**
	 * Creates strip with all LEDs set to the same color, with alpha component used
	 * in calculations.
	 *
	 * @param led_count ammount of LEDs in this strip
	 * @param c color to set all LEDs to
	 */
	public LedStripSingleColor(Integer led_count, Color c)
	{
		this(led_count, c.getRGB());
	}

	@Override
	public String getStringOfBytes()
	{
		StringBuilder sb = new StringBuilder(getByteCount() * 3);
		String s = (String.format("%02x,%02x,%02x;", r, g, b));
		for (int i = 0; i < getLedCount(); i++)
		{
			sb.append(s);

		}
		return sb.toString();

	}

	@Override
	public Color[] getColors()
	{
		Color c[]= new Color[1];
		c[0] = new Color(r,g,b);
		return c;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("StripColor[");
		sb.append("count " + getLedCount()+ ", ");
		sb.append("r" + r + " "); 
		sb.append("g" + g + " ");
		sb.append("b" + b);
		sb.append("]");
		return sb.toString();
	}
}
