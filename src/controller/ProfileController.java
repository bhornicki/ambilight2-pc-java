package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortInvalidPortException;

import javafx.util.Pair;
import strip.screenTranscoder.ScreenCapturer;

/**
 * Uses Profile object to get necessary informations and manage communication
 * with MCU as master
 * 
 * TODO: process multiple awaiting responses situation - now only one response
 * is expected after sending data and no awaiting responses before send is
 * assumed
 * 
 * @author HHV
 *
 */
public class ProfileController {
	
	/**
	 * Delay between acquiring serial port input into SerialPort2 internal buffer
	 */
	static final Integer DELAY_READ_THREAD = 8;
	
	/**
	 * How long will main thread wait until all ports are checked for getting proper
	 * name (MCU ID).
	 */
	static final Integer DELAY_GET_WHOAMI_THREADS_FINISHED = 50;
	
	/**
	 * How long will process maximally wait for response to command until it assumes
	 * broken connection.
	 */
	static final Integer TIMEOUT_COMMAND_REC = 200;
	
	/**
	 * How long will process maximally wait for acknowledging reception of LED data
	 * until it assumes broken connection.
	 */
	static final Integer TIMEOUT_LINE_CONFIRM = 100;
	
	/**
	 * prefix for sending stringOfBytes
	 */
	static final String CMD_SEND_LINE_HEX	= "sethex";
	
	/**
	 * prefix for setting all LEDs to RR,GG,BB hex value
	 */
	static final String CMD_SET_ALL_HEX		= "sendhex";
	
	/**
	 * set led count attached to MCU 
	 */
	static final String CMD_SET_LED_COUNT	= "ledcnt";
	
	/**
	 * Get MCU ID
	 */
	static final String CMD_GET_NAME		= "whoami";
	
	/**
	 * Set minimum time to pass after sending command to let MCU assume no
	 * connection and turn off LEDs.
	 */
	static final String CMD_SET_TIMEOUT		= "timeout";
	
	/**
	 * Should MCU do gamma correction for LEDs
	 */
	static final String CMD_SET_GAMMA		= "gammacorrection";
	
	/**
	 * Everything went okay MCU-side.
	 */
	static final String RESP_OK				= "ok";
	
	/**
	 * Prefix for error message sent from MCU
	 */
	static final String RESP_ERR_PREFIX		= "error ";
	
	public static final String ERR_MCU_NOT_CONNECTED	= "Controller is not connected!";
	public static final String ERR_NULL_PROFILE			= "Null profile!";
	public static final String ERR_OPEN_NULL_PORT 		= "Tried to open NULL Port!";
	public static final String ERR_SET_LED_COUNT		= "Failed to set led count!";
	public static final String ERR_SET_GAMMA_CORR		= "Failed to set gamma correction!";
	public static final String ERR_NO_RESP				= "Controller does not respond!";
	
	/**
	 * Timer used to send LED strip data to MCU and keep proper FPS count.
	 * 
	 * TODO: measure real ammount of FPS and jitter with oscilloscope 
	 */
	Timer timer;


	SerialPort2 serialPort;
	Profile profile;
	
	/**
	 * Determines if controller automatically sends LED data to MCU in timer task
	 */
	Boolean isContinuousRun = false;
	
	/**
	 * Counter of errors during LED data transmission.
	 */
	Integer errorCount=0;
	
	/**
	 * Maximum amount of received errors in a row.
	 */
	static final Integer MAX_ERROR_COUNT = 2;
	
	
	/**
	 * Reads MCU ID from selected serial port
	 * 
	 * @param comPort port to get MCU ID from
	 * @return MCU ID or empty string
	 * @throws SerialPortInvalidPortException passed bad port as an argument
	 */
	static public String getMcuId(String comPort) throws SerialPortInvalidPortException
	{
		SerialPort2 sp = new SerialPort2(comPort, 10, true);
		
		
		// if opening failed - treat as busy
		if(!sp.open())
			return "";
		
		sp.sp.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING | SerialPort.TIMEOUT_READ_BLOCKING, 1000, 1000);
		
		//Send endline and try reading line to empty the mcu receive line buffer  
		sp.writeLine("");
		sp.readLine(TIMEOUT_COMMAND_REC);
		
		//Get MCU ID
		sp.writeLine(CMD_GET_NAME);
		String msg = sp.readLine(TIMEOUT_COMMAND_REC);
		//restore state before opening port
		sp.restoreInitialCloseState();
		
		//If ID compatible with app - add to return list
		if (Profile.isValidMcuId(msg) && msg.startsWith(Profile.MCU_ID_PREFIX))
			return msg;
		return "";
	}

	/**
	 * Reads MCU ID from selected serial port
	 * 
	 * @param comPort port to get MCU ID from
	 * @return MCU ID or empty string
	 * @throws SerialPortInvalidPortException passed bad port as an argument
	 */
	static public String getMcuId(SerialPort comPort) throws SerialPortInvalidPortException
	{
		return getMcuId(comPort.getSystemPortName());
	}

	/**
	 * Reads MCU ID from all connected serial ports
	 * 
	 * @return Array made of Pairs{@literal<}serial port, MCU ID{@literal>}
	 */
	static public ArrayList<Pair<String, String>> getComPortAndMcuId() 
	{
		ArrayList<Pair<String, String>> ret = new ArrayList<Pair<String, String>>(5);
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		SerialPort lut[] = SerialPort.getCommPorts();
		
		for (SerialPort sp : lut) 
			executor.submit(() -> 
			{
				String id = getMcuId(sp);
				if(id.length()>0)
					ret.add(new Pair<String, String>(sp.getSystemPortName(), id));
			});
		
		//wait until all ports are checked.
		while (executor.getActiveCount() > 0)
			try {
				Thread.sleep(DELAY_GET_WHOAMI_THREADS_FINISHED);
			} catch (InterruptedException e) {}
		return ret;
	}

	/**
	 * Sets serialPort to port with connected matching MCU.
	 * 
	 * <pre>
	 * Pseudo code:
	 *  if (comPort is defined)
	 *    send WHOAMI to comPort
	 *    if (mcuId is undefined) and (response is valid MCU ID scheme)
	 *      done, use comPort
	 *    if (mcuId is defined) and (response matches mcuID)
	 *    	done, use comPort
	 *  
	 *  if (mcuId is defined) //and ((comPort undefined) or ((defined comPort) and (response does not match mcuId)))
	 *    send WHOAMI to all ports
	 *    if (response of port equals mcuId)
	 *      done, use this port
	 *    if (no response matches mcuId) and (defined comPort) and (comPort response match MCU ID scheme)
	 *      done, use comPort
	 *  
	 *  if (still not done) 
	 *    throw controller not connected error
	 * </pre>
	 * 
	 * @throws IOException MCU is not connected
	 */
	private void findPort() throws IOException
	{
		String comMcuId="";
		
		if(profile.isValidComPort())
		{
			comMcuId=getMcuId(profile.getComPort());
			if(profile.isValidMcuId() && comMcuId == profile.getMcuId())
			{
				serialPort = new SerialPort2(profile.getComPort(),DELAY_READ_THREAD, true);
				return;
			}
			if(!profile.isValidMcuId() && Profile.isValidMcuId(comMcuId))
			{
				serialPort = new SerialPort2(profile.getComPort(),DELAY_READ_THREAD, true);
				return;
			}
		}
		
		if(profile.isValidMcuId())
		{
			ArrayList<Pair<String, String>> lut = getComPortAndMcuId();
			for (Pair<String, String> p : lut) {
				if (p.getValue().toUpperCase().equals(profile.getMcuId())) {
					serialPort = new SerialPort2(p.getKey(),DELAY_READ_THREAD, true);
					return;
				}
			}
			if(Profile.isValidMcuId(comMcuId))
			{
				serialPort = new SerialPort2(profile.getComPort(),DELAY_READ_THREAD, true);
				return;
			}	
		}
		throw new IOException(ERR_MCU_NOT_CONNECTED);
	}
	
	/**
	 * Constructs new controller
	 * 
	 * 
	 * @param profile profile with data
	 * @throws NullPointerException     no profile passed
	 */
	public ProfileController(Profile profile) throws NullPointerException
	{
		if (profile == null)
			throw new NullPointerException(ERR_NULL_PROFILE);
		this.profile = profile;
		
		
		
		// when app is killed, close port.
//		Runtime.getRuntime().addShutdownHook(new Thread(() -> serialPort.close()));
	}

	/**
	 * Checks profile validity, finds and connects to a controller, sends led count
	 * and gamma correction
	 * 
	 * 
	 * @throws IllegalArgumentException profile is invalid
	 * @throws NullPointerException     no profile is set
	 * @throws IOException              Error occurred during transmission or port
	 *                                  not connected
	 */
	public void start() throws IllegalArgumentException, NullPointerException, IOException
	{
		
		profile.checkValidity();
		findPort();
		if (serialPort == null)
			throw new NullPointerException(ERR_OPEN_NULL_PORT);
		
		serialPort.open();
		serialPort.writeLine(CMD_SET_LED_COUNT + profile.getLedCount());
		String msg = serialPort.readLine(TIMEOUT_COMMAND_REC);
		

		if (!msg.startsWith(RESP_OK)) {
			stop();
			throw new IOException(ERR_SET_LED_COUNT);
		}
		
		serialPort.writeLine(CMD_SET_GAMMA + profile.isGammaCorrectionEnabled());
		if (!msg.startsWith(RESP_OK)) {
			stop();
			throw new IOException(ERR_SET_GAMMA_CORR);
		}
	}
	
	/**
	 * Starts process of automatic sending LED data to MCU
	 */
	public void runContinuous()
	{
		TimerTask timerTask = new TimerTask() {
			@Override
			public void run() {
				ScreenCapturer.syncCapture();
				serialPort.writeLine(CMD_SEND_LINE_HEX + profile.getStripMessage());
				String msg = serialPort.readLine(TIMEOUT_LINE_CONFIRM);

				if (!msg.startsWith(RESP_OK))
				{
					if(errorCount++>=MAX_ERROR_COUNT)
					{
						System.err.println(ERR_NO_RESP);
						isContinuousRun=false;
						stopContinuous();
					}
				}
				else
					errorCount=0;
			}
		};
		timer = new Timer();
		timer.scheduleAtFixedRate(timerTask, 0, 1000 / profile.getFps());
		isContinuousRun=true;
	}
	
	/**
	 * Stops automatic data sending
	 */
	public void stopContinuous()
	{
		if (timer != null)
			timer.cancel();
		timer = null;
		
		if(serialPort != null && serialPort.isOpen())
			serialPort.writeLine(CMD_SET_ALL_HEX+"0,0,0;");
		
		isContinuousRun=false;
	}
	
	/**
	 * Returns true if data sending thread is running and MCU gets constant strip updates
	 * @return true if data sending thread is running
	 */
	public Boolean isRunningContinuous()
	{
		return isContinuousRun;
	}
	
	/**
	 * Sends command and returns response
	 * @param msg command to send
	 * @return MCU response
	 */
	public String sendLine(String msg)
	{
		serialPort.writeLine(msg);
		return serialPort.readLine(TIMEOUT_COMMAND_REC);
	}
	
	/**
	 * Closes the transmission line
	 */
	public void stop() {
		stopContinuous();
		if(serialPort != null)
			serialPort.close();
	}

	@Override
	protected void finalize() throws Throwable {
		stop(); // close port
		super.finalize();
	}
}
