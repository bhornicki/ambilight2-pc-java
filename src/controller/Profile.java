package controller;

import java.io.Serializable;

import strip.LedStrip;
import strip.LedStripSeries;

/**
 * Storage class for application profile.
 * 
 * Profile consists of microcontroller, series of LED strips and some parameters
 * used to make things work
 * 
 * @author HHV
 *
 */
public class Profile implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	/*
	 * controllerName = 24-letter hex unique ID
	 * comName = COMx where x is dec number
	 * 
	 * If comName is specified, will try to open specified port
	 * If controllerName is specified, will open ports and send WHOAMI until response equals controllerName
	 * If both are specified, will open comName port and send WHOAMI. 
	 * 	- if response matches, continue
	 * 	- otherwise open all ports, send WHOAMI 
	 * 	-  - if response matches, continue with proper resp. port (does NOT overwrite comName)
	 *	-  - if no response matches and comName returned valid MCU_ID_PREFIX, continue with comName
	 */
	private String mcuId="";
	private String comPort="";

	private LedStripSeries strips;
	
	/**
	 * Should MCU apply gamma correction
	 */
	private Boolean gammaFix=true;
	
	/**
	 * Frames per second count
	 */
	private Integer targetFps=30;
	
//	TODO implement:
//	private Integer lightIntensity=255;		//LED new_RGB = old_RGB*lightIntensity/255. Would love to use Byte, but Java is retarded and it's not that simple.
//	private Boolean autostart;
	
	/**
	 * Prefix before unique ID used by all compatible MCUs
	 */
	public static final String MCU_ID_PREFIX	= "HHV AMBILIGHT2 STM32-";
	
	public static final String ERR_BAD_MCU_ID				= "Bad MCU ID!";
	public static final String ERR_BAD_COM_PORT				= "Bad serial port!";
	public static final String ERR_INVALID_COM_AND_MCU_ID	= "No valid serial port or MCU ID!";
	public static final String ERR_BAD_FPS					= "Bad FPS count!";
	public static final String ERR_NO_STRIPS				= "No strips in profile!";
	public static final String ERR_INVALID_STRING_OF_BYTES	= "Invalid string of bytes returned!";
	
	public Profile()
	{
		strips = new LedStripSeries();		
	}
	
	/**
	 * Initiates mcuID or comPort depending what string was passed as argument
	 * 
	 * Gamma correction is turned on by default.
	 * 
	 * @param mcuId_or_com MCU ID or "COMx" or "/dev/ttyx"
	 * @throws IllegalArgumentException passed MCU ID or serial port name does not
	 *                                  match pattern
	 */
	public Profile(String mcuId_or_com) throws IllegalArgumentException
	{
		this();
		
		mcuId_or_com=mcuId_or_com.toUpperCase();
		if(isValidComPort(mcuId_or_com))
			setComPort(mcuId_or_com);
		else if(isValidMcuId(mcuId_or_com))
			setMcuId(mcuId_or_com);
		else 
			throw new IllegalArgumentException(ERR_INVALID_COM_AND_MCU_ID);
	}
	
	/**
	 * Initiates mcuID or comPort depending what string was passed as argument. Sets
	 * target Frames Per Second count. 
	 * 
	 * Gamma correction is turned on by default.
	 * 
	 * @param mcuId_or_com MCU ID or "COMx" or "/dev/ttyx"
	 * @param fps          target Frames Per Second count
	 * @throws IllegalArgumentException passed MCU ID or serial port name does not
	 *                                  match pattern. Or FPS count is not valid.
	 */
	public Profile(String mcuId_or_com, Integer fps) throws IllegalArgumentException
	{
		this(mcuId_or_com);
		setFps(fps);
	}
	
	/**
	 * Initiates mcuID or comPort depending what string was passed as argument. Sets
	 * target Frames Per Second count and sets gamma state
	 * 
	 * @param mcuId_or_com          MCU ID or "COMx" or "/dev/ttyx"
	 * @param fps                   target Frames Per Second count
	 * @param enableGammaCorrection should MCU perform gamma correction for LEDs
	 * @throws IllegalArgumentException passed MCU ID or serial port name does not
	 *                                  match pattern. Or FPS count is not valid.
	 */
	public Profile(String mcuId_or_com, Integer fps, Boolean enableGammaCorrection) throws IllegalArgumentException
	{
		this(mcuId_or_com, fps);
		gammaFix = enableGammaCorrection;
	}
	
	
	
	/**
	 * Checks if argument is similar to (MCU_ID_PREFIX + hex) or (hex). Ignores
	 * character case.
	 * 
	 * @param mcuId String to be checked
	 * @return true if argument matches local mcu ID naming scheme
	 */
	public static Boolean isValidMcuId(String mcuId)
	{
		return mcuId.toUpperCase().matches("("+MCU_ID_PREFIX+")?[A-F0-9]{20,24}");
	}
	
	/**
	 * Checks if argument is similar to ("COM" + number) or ("/dev/tty" + number).
	 * Ignores character case.
	 * 
	 * @param comPort String to be checked
	 * @return true if argument matches com port naming scheme
	 */
	public static Boolean isValidComPort(String comPort)
	{
		return comPort.toUpperCase().matches("((COM)|(\\/DEV\\/TTY))[0-9]*");
	}
	
	/**
	 * Checks if object's serialPort is similar to ("COM" + number) or ("/dev/tty" + number).
	 * Ignores character case.
	 * 
	 * @return true if matches com port naming scheme
	 */
	public Boolean isValidComPort()
	{
		return isValidComPort(comPort);
	}
	
	/**
	 * Checks if object's serialPort is similar to (MCU_ID_PREFIX + hex) or (hex).
	 * Ignores character case.
	 * 
	 * @return true if matches com port naming scheme
	 */
	public Boolean isValidMcuId()
	{
		return isValidMcuId(mcuId);
	}
	
	/**
	 * Sets new MCU ID
	 * 
	 * @param mcuId unique ID, unique ID prepended with MCU_ID_PREFIX or empty string
	 * @throws IllegalArgumentException argument is not valid MCU ID, nor is empty
	 */
	public void setMcuId(String mcuId) throws IllegalArgumentException
	{
		if(mcuId.length()!=0 && !isValidMcuId(mcuId))
			throw new IllegalArgumentException(ERR_BAD_MCU_ID);
		
		if(mcuId.startsWith(MCU_ID_PREFIX) || mcuId.length()==0)
			this.mcuId=mcuId;
		else
			this.mcuId=MCU_ID_PREFIX+mcuId;
	}
	
	/**
	 * Sets new serial port
	 * 
	 * @param comPort ("COM" + number), ("/dev/tty" + number) or empty string
	 * @throws IllegalArgumentException argument does not match serial port scheme, nor is empty
	 */
	public void setComPort(String comPort) throws IllegalArgumentException
	{
		if(comPort.length()!=0 && !isValidComPort(comPort))
			throw new IllegalArgumentException(ERR_BAD_COM_PORT);
		
		this.comPort = comPort;
	}
	
	public String getMcuId()
	{
		return mcuId;
	}
	
	public String getComPort()
	{
		return comPort;
	}
	
	public Integer getFps() {
		return targetFps;
	}

	/**
	 * sets target frame rate
	 * @param fps value between 0-255
	 * @throws IllegalArgumentException frame rate is negative or over specified boundary
	 */
	public void setFps(Integer fps) throws IllegalArgumentException
	{
		if(fps<=0 || fps>255)
			throw new IllegalArgumentException(ERR_BAD_FPS);
		targetFps = fps;
	}

	public Boolean isGammaCorrectionEnabled()
	{
		return gammaFix;
	}
	public void setGammaCorrectionEnabled(Boolean isEnabled)
	{
		gammaFix= isEnabled;
	}
	
	public void addStrip(LedStrip strip)
	{
		strips.add(strip);
	}
	
	public LedStripSeries getStrip()
	{
		return strips;
	}
	
	public String getStripMessage()
	{
		return strips.getStringOfBytes();
	}
	public Integer getLedCount()
	{
		return strips.getLedCount();
	}
	
	/**
	 * Checks if profile is valid
	 * 
	 * If profile contains invalid MCU ID or invalid serial port, throws exception.
	 * 
	 * If profile does not specify MCU ID and serial port, throws error.
	 * 
	 * If FPS count does not match 0-255 values, you know what happens.
	 * 
	 * If no strips are defined - exception.
	 * 
	 * If any strip returns invalid stringOfBytes (bad formatting, not enough
	 * values) - exception.
	 * 
	 * @throws IllegalArgumentException profile does not fulfill one of specified conditions
	 */
	public void checkValidity() throws IllegalArgumentException
	{
		if(mcuId.length()>0 && !isValidMcuId())
			throw new IllegalArgumentException(ERR_BAD_MCU_ID);
		if(comPort.length()>0 && !isValidComPort())
			throw new IllegalArgumentException(ERR_BAD_COM_PORT);
		if(mcuId.length()<=0 && comPort.length()<=0)
			throw new IllegalArgumentException(ERR_INVALID_COM_AND_MCU_ID);
		if(targetFps<=0 || targetFps>255)
			throw new IllegalArgumentException(ERR_BAD_FPS);

		String bytes = strips.getStringOfBytes();
		Integer count = strips.getLedCount();
		if(bytes.length()==0)
			throw new IllegalArgumentException(ERR_NO_STRIPS);
		if(!bytes.matches("(((0x|X)?[A-Fa-f0-9]{1,2},){2}(0x|X)?[A-Fa-f0-9]{1,2};){"+count+"}"))
		{
			System.err.println(count);
			System.err.println(bytes);
			throw new IllegalArgumentException(ERR_INVALID_STRING_OF_BYTES);
		}
	}


	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Profile[");
		if(mcuId.length()>0)
			sb.append("ID \"" + mcuId+"\", ");
		if(comPort.length()>0)
			sb.append("comPort \"" + comPort + "\", ");
		sb.append("gamma correction " + gammaFix + ", ");
		sb.append("FPS " + targetFps + "]");
		return sb.toString();
	}
	
	
	
}
