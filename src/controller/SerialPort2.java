package controller;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortInvalidPortException;

/**
 * Wrapper for jSerialComm SerialPort.
 * 
 * Delegates sending messages via serial port and buffers received messages.
 * 
 * @author HHV
 *
 */
public class SerialPort2 
{
	
	SerialPort sp;

	/**
	 * receive buffer
	 */
	StringBuilder sbRx;	
	
	/**
	 * initial state of port
	 */
	Boolean wasOpen;
	
	/**
	 * Send line ending style: \n or \r\n
	 */
	Boolean isWriteLineLfOnly=true;
	
	/**
	 * How often update received buffer
	 */
	Integer RECEIVE_BUFFER_UPDATE_DELAY;
	
	/**
	 * Receive buffer "synchronizer" thread 
	 */
	Thread tRx;
	
	/**
	 * Constructor
	 * 
	 * @param port serial port to be opened
	 * @param receiveBufferUpdateDelay how often should receive buffer be synchronized
	 * @param isWriteLineLfOnly line ending style
	 * @throws SerialPortInvalidPortException passed bad port name
	 */
	public SerialPort2(String port, Integer receiveBufferUpdateDelay, Boolean isWriteLineLfOnly) throws SerialPortInvalidPortException
	{
		sp = SerialPort.getCommPort(port);
		sbRx = new StringBuilder();
		wasOpen=sp.isOpen();
		RECEIVE_BUFFER_UPDATE_DELAY=receiveBufferUpdateDelay;
		this.isWriteLineLfOnly=isWriteLineLfOnly;
	}
	
	public Boolean isOpen()
	{
		return sp.isOpen();
	}
	
	/**
	 * Opens serial port
	 * 
	 * TODO: change println to exceptions
	 * 
	 * @return true if operation succeeded
	 */
	public Boolean open()
	{
		if(sp.isOpen()||!sp.openPort())
		{
			System.err.println(getName()+" busy!");
			return false;
		};
//		while(!sp.isOpen());
		tRx = new Thread(() -> 
		{
			while(true)
			{				
				try {
						if(!isOpen())
							return;
					while(sp.getInputStream().available()>0)
					{
						char chr = (char) sp.getInputStream().read();
						if(chr == -1) 
							break;
						if(chr=='\r')
							continue;
						if(chr=='\n' && sbRx.charAt(sbRx.length()-1)=='\n')
							continue;
							
						sbRx.append(chr);
					}
				} catch (NullPointerException | IOException e) {
					System.err.println(getName()+" not avilable!");		
					e.printStackTrace();
					break;
				}
				
				try {
					Thread.sleep(RECEIVE_BUFFER_UPDATE_DELAY);
				} catch (InterruptedException e) {
					return;
				}
			}
		});
		tRx.start();
		return true;
	}
	
	/**
	 * Constructor, send \n endline style
	 * 
	 * @param port serial port to be opened
	 * @param receiveBufferUpdateDelay how often should receive buffer be synchronized
	 * @throws SerialPortInvalidPortException passed bad port name
	 */
	public SerialPort2(String port, Integer receiveBufferUpdateDelay) throws SerialPortInvalidPortException
	{
		this(port, receiveBufferUpdateDelay, true);
	}
	
	/**
	 * Constructor, send \n endline style, 10ms synchronization delay
	 * 
	 * @param port serial port to be opened
	 * @throws SerialPortInvalidPortException passed bad port name
	 */
	public SerialPort2(String port) throws SerialPortInvalidPortException
	{
		this(port, 10, true);
	}
	
	/**
	 * Closes port
	 */
	public void close()
	{
		tRx.interrupt();
		while(tRx.isAlive());
		sp.closePort();
		clearReadBuffer();
		sbRx.trimToSize();
	}
	
	/**
	 * Returns port name
	 * 
	 * @return "COMx" or "/dev/ttyx"
	 */
	public String getName()
	{
		return sp.getSystemPortName();
	}
	
	/**
	 * Restores port state which was before call of SerialPort2 constructor
	 */
	public void restoreInitialCloseState()
	{
		if(isOpen()&&!wasOpen)
			sp.closePort();
		else if((!isOpen())&&wasOpen)
			sp.openPort();
	}
	
	public final int writeBytes(byte[] buffer, long bytesToWrite)
	{
		return sp.writeBytes(buffer, bytesToWrite);
	}
	
	public void writeString(String msg)
	{
		writeBytes(msg.getBytes(), msg.length());
	}
	
	/**
	 * Ignores all newlines and carriage returns in message and sends it with
	 * endline style selected in class
	 * 
	 * @param msg line to send
	 */
	public void writeLine(String msg)
	{
		writeString(msg.replace("\r", "").replace("\n", "")+(isWriteLineLfOnly?"\n":"\r\n"));
	}
	
	/**
	 * Returns received line or empty string if line was not finished by newline
	 * 
	 * @return received line
	 */
	public String readLine()
	{
		Integer idx = sbRx.indexOf("\n");
		if(idx==-1)
			return "";
		String ret=sbRx.substring(0, idx);
		sbRx.delete(0, idx+1);
		return ret;
	}
	
	/**
	 * Returns received line or empty string if line was not finished by newline.
	 * 
	 * Awaits for line reception up to timeout milliseconds
	 * 
	 * @param timeout how long maximally wait for new line in ms
	 * @return received line
	 */
	public String readLine(Integer timeout) {
		final ExecutorService executor = Executors.newSingleThreadExecutor();
		final Future<String> future = executor.submit(() -> {
			while (getReadLineLength()<1)
				Thread.sleep(RECEIVE_BUFFER_UPDATE_DELAY);
			return readLine();
		});
		String ret = "";
		try {
			ret = future.get(timeout, TimeUnit.MILLISECONDS);
		} catch (Exception e) {}

		executor.shutdownNow();
		return ret;
	}

	
	public void clearReadBuffer()
	{
		sbRx.delete(0, sbRx.length());
	}
	
	/**
	 * How many character will next line have or -1 when no finished line is present.
	 * 
	 * @return How many character will next line have or -1 when no finished line is present.
	 */
	public Integer getReadLineLength()
	{
		return sbRx.indexOf("\n");
	}
}
